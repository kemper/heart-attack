types = require('./lib/hearts_types')
Deck = require('./deck')
Hand = require('./hand')
Cards = require('./cards')
History = require('./history')
Position = types.Position
Rank = types.Rank
Suit = types.Suit

class Player
  constructor: (@position) ->
    @history = new History

  receiveHand: (@hand) ->
    @history.setPlayerCards(@hand, @position)

  log: (message, object=undefined) ->
    if object?
      console.log message, object
    else
      console.log message

  passCards: (direction) ->
    cardsToPass = @hand.cardsToPass()
    @hand.removeAll(cardsToPass)
    cardsToPass.cards

  receivePassedCards: (cards) ->
    @hand.addAll(cards)

  playTrick: (trickNumber, roundNumber, previousCards, startingPosition) ->
    @trickNumber = trickNumber
    @history.record(trickNumber, startingPosition, previousCards)
    if @hand.hasTwoOfClubs()
      cardToPlay = @hand.twoOfClubs()
    else if previousCards.size() > 0
      cardToPlay = @followPlay(previousCards)
    else
      cardToPlay = @leadTrick()

    @hand.remove(cardToPlay)
    cardToPlay

  recordResult: (startingPosition, cards) ->
    @history.record(@trickNumber, startingPosition, cards)

  #private methods

  leadTrick: () ->
    @selectCardToLead(@hand)

  selectCardToLead: (hand) ->
    cardToPlay = undefined
    spades = new Cards hand.all(Suit.SPADES)
    if spades.size() > 0 and !spades.has(Deck.queenOfSpades) and !spades.has(Deck.kingOfSpades) and !spades.has(Deck.aceOfSpades)
      cardToPlay = spades.highest()
    else
      cards = new Cards
      cards.addAll new Cards hand.all(Suit.CLUBS)
      cards.addAll new Cards hand.all(Suit.DIAMONDS)
      if @history.heartsBroken()
        cards.addAll new Cards(hand.all(Suit.HEARTS))
      cardToPlay = cards.lowest()

    unless cardToPlay? #have to play spades
      cardToPlay = hand.lowest()

    cardToPlay

  followPlay: (previousCards) ->
    cardToPlay = undefined
    firstPlayed = previousCards.first()
    if @hand.all(firstPlayed.suit).length > 0
      cardToPlay = @playSameSuit(firstPlayed, previousCards)
    else
      cardToPlay = @playOffSuit()

    cardToPlay

  playOffSuit: ->
    cardToPlay = undefined
    spades = new Cards @hand.all(Suit.SPADES)
    nonSpades = new Cards
    nonSpades.addAll new Cards(@hand.all(Suit.CLUBS))
    nonSpades.addAll new Cards(@hand.all(Suit.DIAMONDS))

    if spades.size() > 0 and !spades.has(Deck.queenOfSpades) and (spades.has(Deck.kingOfSpades) or spades.has(Deck.aceOfSpades))
      cardToPlay = spades.highest()
    else if @trickNumber == 1
      cardToPlay = nonSpades.highest()
    else if @hand.hasQueenOfSpades()
      cardToPlay = @hand.queenOfSpades()
    else
      nonSpades.addAll new Cards(@hand.all(Suit.HEARTS))
      cardToPlay ||= nonSpades.highest()

    unless cardToPlay?
      cardToPlay = @hand.highest()

    cardToPlay

  playSameSuit: (firstPlayed, played) ->
    console.log("about to play same suit")
    cardToPlay = undefined
    suit = firstPlayed.suit
    highestPlayed = played.highest(suit)
    undercard = @hand.highestUnder(highestPlayed)
    myLowest = @hand.lowest(suit)
    myHighest = @hand.highest(suit)
    hasManyOfSuit = @hand.all(suit).length > 1

    # for projecting next hand
    nextHand = new Hand @hand.cards
    nextHand.remove(myHighest)
    nextCardToPlay = @selectCardToLead(nextHand)
    canLoseNextRound = @history.likelyToLose(nextCardToPlay, new Cards) if nextCardToPlay

    if @trickNumber == 1
      cardToPlay = myHighest
    else if suit == Suit.SPADES
      if @hand.has(Deck.queenOfSpades)
        if (played.has(Deck.aceOfSpades) || played.has(Deck.kingOfSpades))
          cardToPlay = @hand.queenOfSpades()
        if hasManyOfSuit
          if @hand.has(Deck.kingOfSpades)
            cardToPlay = Deck.kingOfSpades
          else
            cardToPlay = @hand.highestUnder(Deck.queenOfSpades)
            if !cardToPlay? && @hand.has(Deck.aceOfSpades)
              cardToPlay = Deck.aceOfSpades
        else
          cardToPlay = Deck.queenOfSpades
      else # I don't have the queen
        if played.size() == 3
          if (myHighest == Deck.kingOfSpades or myHighest == Deck.aceOfSpades) || canLoseNextRound
            cardToPlay = myHighest
          else
            cardToPlay = undercard
        else
          if canLoseNextRound
            cardToPlay = @hand.highestUnder(Deck.queenOfSpades)
          else
            cardToPlay = undercard
        cardToPlay = myLowest unless cardToPlay?
    else if played.points() > 0
      if undercard?
        cardToPlay = undercard
      else if played.size() == 3
        cardToPlay = myHighest
      else
        cardToPlay = myLowest
    else # not first round, no points yet
      if myHighest != Deck.queenOfSpades and nextCardToPlay and played.size() == 3 and canLoseNextRound # if I can give up control, take it
        cardToPlay = myHighest
      else if undercard?
        cardToPlay = undercard
      else if @history.likelyToLose(myLowest, played)
        cardToPlay = myLowest
      else
        cardToPlay = myHighest

    cardToPlay

  likelyToLostNextRound: (@hand, myHighest) ->


module.exports = Player
