thrift = require('thrift')
Hearts = require('./lib/Hearts')
types = require('./lib/hearts_types')
Position = types.Position

Hand = require('./hand')
Deck = require('./deck')
Cards = require('./cards')
Player = require('./player')

connection = thrift.createConnection('localhost', 4001)
client = thrift.createClient(Hearts, connection)

class Agent
  constructor: (@game) ->

  log: (message, object=undefined) ->
    if object?
      console.log message, object
    else
      console.log message

  result: (message, object=undefined) ->
    if object?
      console.log message, object
    else
      console.log message

  run: ->
    request = new types.EntryRequest()
    console.log "Entering arena", request
    @game.enter_arena request, (err, response) =>
      throw err if err
      @ticket = response.ticket
      if @ticket
        @play()

  play: ->
    @log "playing"

    @game.get_game_info @ticket, (err, gameInfo) =>
      throw err if err
      @result "game info:", gameInfo
      @gameInfo = gameInfo
      @roundNumber = 0
      @playRound()

  positionString: (position) ->
    map = {}
    map[Position.NORTH] = "N"
    map[Position.EAST] = "E"
    map[Position.SOUTH] = "S"
    map[Position.WEST] = "W"
    map[position]

  playRound: ->
    @player = new Player(@gameInfo.position)
    @roundNumber += 1
    @game.get_hand @ticket, (err, hand) =>
      throw err if err
      thehand = Deck.lookupAll(hand)
      thehand.sort()
      @log "hand:", thehand.toString()
      @hand = new Hand(thehand.items)
      @player.receiveHand(@hand)

      if @roundNumber % 4 != 0
        cardsToPass = @player.passCards()
        @log "Passing Cards:", cardsToPass.toString()
        cardsToPass = Deck.reverseLookupAll(cardsToPass)
        @game.pass_cards @ticket, cardsToPass, (err, receivedCards) =>
          throw err if err
          @log "Received Cards:", Deck.lookupAll(receivedCards).toString()
          @player.receivePassedCards(Deck.lookupAll(receivedCards))
          @log "Final Cards for this round:", @hand.toString()
          @playTrick(0)
      else
        @playTrick(0)

  playTrick: (trickNumber) ->
    #@log "[#{@positionString(@gameInfo.position)}, round #{@roundNumber}, trick #{trickNumber}, playing trick"

    @game.get_trick @ticket, (err, trick) =>
      throw err if err
      cardsPlayed = Deck.lookupAll(trick.played)
      @log "cards played:", cardsPlayed.toString()

      cardToPlay = @player.playTrick(trickNumber, @roundNumber, cardsPlayed, trick.leader)
      @log "#{@positionString(@gameInfo.position)} playing card:", cardToPlay.toString()

      cardToPlay = Deck.reverseLookup(cardToPlay)

      @game.play_card @ticket, cardToPlay, (err, trickResult) =>
        throw err if err
        cardsPlayed = Deck.lookupAll(trickResult.played)
        @player.recordResult trickResult.leader, cardsPlayed

        @result "trick result: #{cardsPlayed.toString()}"

        if trickNumber >= 12
          @game.get_round_result @ticket, (err, roundResult) =>
            throw err if err
            @result "round result:", roundResult
            if roundResult.status != types.GameStatus.NEXT_ROUND
              @game.get_game_result @ticket, (err, gameResult) =>
                throw err if err
                @result "game result:", gameResult
                connection.end()
            else
              @playRound()

        else
          @playTrick trickNumber + 1


bot = new Agent(client)
bot.run()

