class Collection
  constructor: (items) ->
    @items = (items && items.concat([])) || []

  add: (item) ->
    if item != undefined && item != null
      @items.push(item) unless @has(item)

  remove: (item) ->
    index = @items.indexOf item
    @items.splice(index, 1) if @has(item)

  removeAll: (items) ->
    @remove(item) for item in items.items

  addAll: (items) ->
    @add(item) for item in items.items

  has: (item) ->
    @items.indexOf(item) != -1

  size: ->
    @items.length

  first: ->
    @items[0]

  toString: ->
    item.toString() for item in @items

module.exports = Collection
