types = require('./lib/hearts_types')
Deck = require('./deck')
Cards = require('./cards')
Collection = require('./collection')

#
# Holds arrays with 13 elements
# Who started
# Who won
# How many point were taken
# Who played what (including main player)
#
# or, holds a single list with objects that have each of these things
#
# can update current round
#
# can tell me:
#   what suits each player is out of
#   how many points each player has (total and this round)
#

class History
  north: types.Position.NORTH
  east: types.Position.EAST
  south: types.Position.SOUTH
  west: types.Position.WEST

  hearts: types.Suit.HEARTS
  spades: types.Suit.SPADES
  diamonds: types.Suit.DIAMONDS
  clubs: types.Suit.CLUBS

  suitsByPosition: {}
  cardsByPosition: {}
  nextPosition: {}

  constructor: ->
    @nextPosition[@north] = @east
    @nextPosition[@east] = @south
    @nextPosition[@south] = @west
    @nextPosition[@west] = @north

    @cardsByPosition[@north] = new Cards
    @cardsByPosition[@east] = new Cards
    @cardsByPosition[@south] = new Cards
    @cardsByPosition[@west] = new Cards

    @suitsByPosition[@north] = new Collection [@clubs, @diamonds, @spades, @hearts]
    @suitsByPosition[@east] = new Collection [@clubs, @diamonds, @spades, @hearts]
    @suitsByPosition[@south] = new Collection [@clubs, @diamonds, @spades, @hearts]
    @suitsByPosition[@west] = new Collection [@clubs, @diamonds, @spades, @hearts]

  toString: ->
    "Player Position: #{@playerPosition.toString()}\n" + 
    "Player Hand: #{@playerCards.toString()}\n" +
    "North Cards: #{@cardsByPosition[@north].toString()}\n" +
    "North Suits: #{@suitsByPosition[@north].toString()}\n" +
    "East Cards: #{@cardsByPosition[@east].toString()}\n" +
    "East Suits: #{@suitsByPosition[@east].toString()}\n" +
    "South Cards: #{@cardsByPosition[@south].toString()}\n" +
    "South Suits: #{@suitsByPosition[@south].toString()}\n" +
    "West Cards: #{@cardsByPosition[@west].toString()}\n" +
    "West Suits: #{@suitsByPosition[@west].toString()}\n"

  cardsFor: (position) ->
    @cardsByPosition[position]

  record: (roundNumber, startingPosition, cards) ->
    return if cards.size() == 0
    @recordSuits(startingPosition, cards)
    @recordPlayersSuits()
    @recordCards(roundNumber, startingPosition, cards)
    @recordEmptySuits()

  setPlayerCards: (@playerCards, @playerPosition) ->

  likelyToLose: (card, cardsPlayed) ->
    @cardToSuitRatio(card, cardsPlayed) < 1

  cardToSuitRatio: (card, cardsPlayed) ->
    remaining = @remainingCards()
    toConsider = 3 - cardsPlayed.size()
    numberOfSuits = @countSuitsRemaining(card.suit, toConsider)
    numberOfUndercards = remaining.under(card).size()
    numberOfUndercards / numberOfSuits

  certainToLose: (card) ->
    @remainingCards().lowest(card.suit) == card

  heartsBroken: ->
    @playedCards().all(@hearts).length > 0

  # private methods

  recordCards: (roundNumber, startingPosition, cards) ->
    position = startingPosition
    for card in cards.cards
      @cardsByPosition[position].add(card)
      position = @nextPosition[position]

  recordSuits: (startingPosition, cards) ->
    startingSuit = cards.first().suit
    position = startingPosition
    for card in cards.cards
      @suitsByPosition[position].remove(startingSuit) unless card.suit == startingSuit
      position = @nextPosition[position]

  recordEmptySuits: () ->
    suits = new Collection Deck.allSuits
    suits.removeAll @remainingSuits()
    position = @nextPosition[@playerPosition]
    for i in [1..3]
      @suitsByPosition[position].removeAll suits
      position = @nextPosition[position]

  recordPlayersSuits: () ->
    @suitsByPosition[@playerPosition] = @playerCards.suits()

  possibleSuits: (position) ->
    @suitsByPosition[position]


  remainingCards: () ->
    remaining = new Cards Deck.allCards
    remaining.removeAll @playedCards()
    remaining.removeAll @playerCards
    remaining

  playedCards: () ->
    played = new Cards
    played.addAll @cardsFor(@north)
    played.addAll @cardsFor(@east)
    played.addAll @cardsFor(@south)
    played.addAll @cardsFor(@west)
    played

  remainingSuits: ->
    @remainingCards().suits()

  countSuitsRemaining: (suit, remainingPlayers) ->
    suitsRemaining = 0
    position = @nextPosition[@playerPosition]
    for i in [1..remainingPlayers]
      suits = @suitsByPosition[position]
      suitsRemaining += 1 if suits.has suit
      position = @nextPosition[position]
    suitsRemaining


module.exports = History
