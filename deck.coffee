types = require('./lib/hearts_types')
Rank = types.Rank
Suit = types.Suit
Card = require('./card')
Cards = require('./cards')

class Deck
  @totalCards = 52
  @aceOfSpades:   new Card {rank: Rank.ACE,   suit: Suit.SPADES}
  @kingOfSpades:  new Card {rank: Rank.KING,  suit: Suit.SPADES}
  @queenOfSpades: new Card {rank: Rank.QUEEN, suit: Suit.SPADES}
  @jackOfSpades:  new Card {rank: Rank.JACK,  suit: Suit.SPADES}
  @tenOfSpades:   new Card {rank: Rank.TEN,   suit: Suit.SPADES}
  @nineOfSpades:  new Card {rank: Rank.NINE,  suit: Suit.SPADES}
  @eightOfSpades: new Card {rank: Rank.EIGHT, suit: Suit.SPADES}
  @sevenOfSpades: new Card {rank: Rank.SEVEN, suit: Suit.SPADES}
  @sixOfSpades:   new Card {rank: Rank.SIX,   suit: Suit.SPADES}
  @fiveOfSpades:  new Card {rank: Rank.FIVE,  suit: Suit.SPADES}
  @fourOfSpades:  new Card {rank: Rank.FOUR,  suit: Suit.SPADES}
  @threeOfSpades: new Card {rank: Rank.THREE, suit: Suit.SPADES}
  @twoOfSpades:   new Card {rank: Rank.TWO,   suit: Suit.SPADES}

  @aceOfHearts:   new Card {rank: Rank.ACE,   suit: Suit.HEARTS}
  @kingOfHearts:  new Card {rank: Rank.KING,  suit: Suit.HEARTS}
  @queenOfHearts: new Card {rank: Rank.QUEEN, suit: Suit.HEARTS}
  @jackOfHearts:  new Card {rank: Rank.JACK,  suit: Suit.HEARTS}
  @tenOfHearts:   new Card {rank: Rank.TEN,   suit: Suit.HEARTS}
  @nineOfHearts:  new Card {rank: Rank.NINE,  suit: Suit.HEARTS}
  @eightOfHearts: new Card {rank: Rank.EIGHT, suit: Suit.HEARTS}
  @sevenOfHearts: new Card {rank: Rank.SEVEN, suit: Suit.HEARTS}
  @sixOfHearts:   new Card {rank: Rank.SIX,   suit: Suit.HEARTS}
  @fiveOfHearts:  new Card {rank: Rank.FIVE,  suit: Suit.HEARTS}
  @fourOfHearts:  new Card {rank: Rank.FOUR,  suit: Suit.HEARTS}
  @threeOfHearts: new Card {rank: Rank.THREE, suit: Suit.HEARTS}
  @twoOfHearts:   new Card {rank: Rank.TWO,   suit: Suit.HEARTS}

  @aceOfClubs:   new Card {rank: Rank.ACE,   suit: Suit.CLUBS}
  @kingOfClubs:  new Card {rank: Rank.KING,  suit: Suit.CLUBS}
  @queenOfClubs: new Card {rank: Rank.QUEEN, suit: Suit.CLUBS}
  @jackOfClubs:  new Card {rank: Rank.JACK,  suit: Suit.CLUBS}
  @tenOfClubs:   new Card {rank: Rank.TEN,   suit: Suit.CLUBS}
  @nineOfClubs:  new Card {rank: Rank.NINE,  suit: Suit.CLUBS}
  @eightOfClubs: new Card {rank: Rank.EIGHT, suit: Suit.CLUBS}
  @sevenOfClubs: new Card {rank: Rank.SEVEN, suit: Suit.CLUBS}
  @sixOfClubs:   new Card {rank: Rank.SIX,   suit: Suit.CLUBS}
  @fiveOfClubs:  new Card {rank: Rank.FIVE,  suit: Suit.CLUBS}
  @fourOfClubs:  new Card {rank: Rank.FOUR,  suit: Suit.CLUBS}
  @threeOfClubs: new Card {rank: Rank.THREE, suit: Suit.CLUBS}
  @twoOfClubs:   new Card {rank: Rank.TWO,   suit: Suit.CLUBS}

  @aceOfDiamonds:    new Card {rank: Rank.ACE,   suit: Suit.DIAMONDS}
  @kingOfDiamonds:   new Card {rank: Rank.KING,  suit: Suit.DIAMONDS}
  @queenOfDiamonds:  new Card {rank: Rank.QUEEN, suit: Suit.DIAMONDS}
  @jackOfDiamonds:   new Card {rank: Rank.JACK,  suit: Suit.DIAMONDS}
  @tenOfDiamonds:    new Card {rank: Rank.TEN,   suit: Suit.DIAMONDS}
  @nineOfDiamonds:   new Card {rank: Rank.NINE,  suit: Suit.DIAMONDS}
  @eightOfDiamonds:  new Card {rank: Rank.EIGHT, suit: Suit.DIAMONDS}
  @sevenOfDiamonds:  new Card {rank: Rank.SEVEN, suit: Suit.DIAMONDS}
  @sixOfDiamonds:    new Card {rank: Rank.SIX,   suit: Suit.DIAMONDS}
  @fiveOfDiamonds:   new Card {rank: Rank.FIVE,  suit: Suit.DIAMONDS}
  @fourOfDiamonds:   new Card {rank: Rank.FOUR,  suit: Suit.DIAMONDS}
  @threeOfDiamonds:  new Card {rank: Rank.THREE, suit: Suit.DIAMONDS}
  @twoOfDiamonds:    new Card {rank: Rank.TWO,   suit: Suit.DIAMONDS}

  @allCards: [
    @aceOfSpades,      @aceOfHearts,      @aceOfClubs,      @aceOfDiamonds,
    @kingOfSpades,     @kingOfHearts,     @kingOfClubs,     @kingOfDiamonds,
    @queenOfSpades,    @queenOfHearts,    @queenOfClubs,    @queenOfDiamonds,
    @jackOfSpades,     @jackOfHearts,     @jackOfClubs,     @jackOfDiamonds,
    @tenOfSpades,      @tenOfHearts,      @tenOfClubs,      @tenOfDiamonds,
    @nineOfSpades,     @nineOfHearts,     @nineOfClubs,     @nineOfDiamonds,
    @eightOfSpades,    @eightOfHearts,    @eightOfClubs,    @eightOfDiamonds,
    @sevenOfSpades,    @sevenOfHearts,    @sevenOfClubs,    @sevenOfDiamonds,
    @sixOfSpades,      @sixOfHearts,      @sixOfClubs,      @sixOfDiamonds,
    @fiveOfSpades,     @fiveOfHearts,     @fiveOfClubs,     @fiveOfDiamonds,
    @fourOfSpades,     @fourOfHearts,     @fourOfClubs,     @fourOfDiamonds,
    @threeOfSpades,    @threeOfHearts,    @threeOfClubs,    @threeOfDiamonds,
    @twoOfSpades,      @twoOfHearts,      @twoOfClubs,      @twoOfDiamonds]

  @allSuits = [
    Suit.DIAMONDS,
    Suit.CLUBS,
    Suit.HEARTS,
    Suit.SPADES
  ]

  @lookup: (in_card) ->
    (card for card in @allCards when card.suit == in_card.suit && card.rank == in_card.rank)[0]

  @lookupAll: (cards) ->
    new Cards(@lookup(card) for card in cards)

  @reverseLookupAll: (cards) ->
    @reverseLookup(card) for card in cards

  @reverseLookup: (card) ->
    new types.Card {rank: card.rank, suit: card.suit}

module.exports = Deck
