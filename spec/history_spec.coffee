History = require('../history')
types = require('../lib/hearts_types')
Deck = require('../deck')
Hand = require('../hand')
Cards = require('../cards')
Rank = types.Rank
Suit = types.Suit
Position = types.Position

describe "history", ->
  north = Position.NORTH
  east = Position.EAST
  south = Position.SOUTH
  west = Position.WEST

  hearts = Suit.HEARTS
  spades = Suit.SPADES
  diamonds = Suit.DIAMONDS
  clubs = Suit.CLUBS

  beforeEach ->
    @history = new History
    @history.setPlayerCards(new Cards, south)
    @cards1 = new Cards [
      Deck.kingOfDiamonds,
      Deck.queenOfDiamonds,
      Deck.jackOfDiamonds,
      Deck.tenOfDiamonds
    ]
    @cards2 = new Cards [
      Deck.threeOfHearts,
      Deck.twoOfHearts,
      Deck.fourOfHearts,
      Deck.aceOfHearts
    ]
    @cards3 = new Cards [
      Deck.twoOfSpades,
      Deck.threeOfSpades,
      Deck.fourOfSpades,
      Deck.aceOfSpades
    ]
    # this represents an incomplete round
    @cards4 = new Cards [
      Deck.sixOfClubs,
      Deck.fiveOfClubs
    ]

  describe "record", ->
    it "should add cards by player, including incomplete card rounds", ->
      @history.record(1, north, @cards1)
      @history.record(2, north, @cards2)
      @history.record(3, west, @cards3)
      @history.record(4, south, @cards4)

      northCards = @history.cardsFor(Position.NORTH)
      eastCards = @history.cardsFor(Position.EAST)
      southCards = @history.cardsFor(Position.SOUTH)
      westCards = @history.cardsFor(Position.WEST)

      expectedNorthCards = new Cards [
        Deck.kingOfDiamonds,
        Deck.threeOfHearts,
        Deck.threeOfSpades
      ]
      expectedEastCards = new Cards [
        Deck.queenOfDiamonds,
        Deck.twoOfHearts,
        Deck.fourOfSpades
      ]
      expectedSouthCards = new Cards [
        Deck.jackOfDiamonds,
        Deck.fourOfHearts,
        Deck.aceOfSpades,
        Deck.sixOfClubs
      ]
      expectedWestCards = new Cards [
        Deck.tenOfDiamonds,
        Deck.aceOfHearts,
        Deck.twoOfSpades
        Deck.fiveOfClubs
      ]

      expect(northCards.cards).toEqual(expectedNorthCards.cards)
      expect(eastCards.cards).toEqual(expectedEastCards.cards)
      expect(southCards.cards).toEqual(expectedSouthCards.cards)
      expect(westCards.cards).toEqual(expectedWestCards.cards)

    it "should know the remaining cards, considering the players hand", ->
      @history.setPlayerCards @cards1, south
      @history.record 1, north, @cards2
      remaining = @history.remainingCards()
      expect(remaining.size()).toBe(44)

    it "should know all cards played", ->
      @history.record 1, north, @cards1
      expect(@history.playedCards().cards).toEqual(@cards1.cards)

    it "should track when a player is out of a suit", ->
      cards = new Cards [
        Deck.kingOfDiamonds,
        Deck.queenOfDiamonds,
        Deck.aceOfHearts,
        Deck.tenOfDiamonds
      ]

      @history.setPlayerCards(new Cards, west)
      @history.record 1, north, cards
      expect(@history.possibleSuits(north).items).toEqual([clubs, diamonds, spades, hearts])
      expect(@history.possibleSuits(east).items).toEqual([clubs, diamonds, spades, hearts])
      expect(@history.possibleSuits(south).items).toEqual([clubs, spades, hearts])
      expect(@history.possibleSuits(west).items).toEqual([])

    it "should know that the other players are out of a suit when the player holds the remaining cards in a suit", ->
      cards1 = new Cards [
        Deck.aceOfDiamonds,
        Deck.kingOfDiamonds,
        Deck.queenOfDiamonds,
        Deck.jackOfDiamonds
      ]
      cards2 = new Cards [
        Deck.tenOfDiamonds,
        Deck.nineOfDiamonds,
        Deck.eightOfDiamonds,
        Deck.sevenOfDiamonds
      ]
      hand = new Hand [
        Deck.sixOfDiamonds,
        Deck.fiveOfDiamonds,
        Deck.fourOfDiamonds,
        Deck.threeOfDiamonds,
        Deck.twoOfDiamonds
      ]

      @history.setPlayerCards(hand, west)
      @history.record 1, north, cards1
      @history.record 1, north, cards2
      expect(@history.possibleSuits(north).items).toEqual([clubs, spades, hearts])
      expect(@history.possibleSuits(east).items).toEqual([clubs, spades, hearts])
      expect(@history.possibleSuits(south).items).toEqual([clubs, spades, hearts])
      expect(@history.possibleSuits(west).items).toEqual([diamonds])

  describe "certainToLose", ->
    beforeEach ->
      @cards1 = new Cards [
        Deck.aceOfDiamonds,
        Deck.kingOfDiamonds,
        Deck.queenOfDiamonds,
        Deck.jackOfDiamonds
      ]
      @cards2 = new Cards [
        Deck.tenOfDiamonds,
        Deck.nineOfDiamonds,
        Deck.eightOfDiamonds,
        Deck.sevenOfDiamonds,
      ]
      @cards3 = new Cards [
        Deck.sixOfDiamonds,
        Deck.aceOfHearts,
        Deck.fiveOfDiamonds,
        Deck.fourOfDiamonds
      ]
      @playerCards = new Cards [Deck.fiveOfSpades]
      @playerPosition = west

    it "returns true when it's the lowest remaining card of a suit and other players still have the suit", ->
      @history.setPlayerCards(@playerCards, @playerPosition)
      @history.record 1, north, @cards1
      @history.record 2, north, @cards2
      @history.record 3, north, @cards3
      expect(@history.certainToLose(Deck.twoOfDiamonds)).toBe(true)

    it "returns false when it's the highest card of a suit", ->
      @history.setPlayerCards(@playerCards, @playerPosition)
      @history.record 1, north, @cards1
      @history.record 2, north, @cards2
      @history.record 3, north, @cards3
      expect(@history.certainToLose(Deck.threeOfDiamonds)).toBe(false)

    it "returns false when the other players are all out of the suit", ->
      @history.setPlayerCards(new Cards [Deck.twoOfDiamonds, Deck.threeOfDiamonds], west)
      @history.record 1, north, @cards1
      @history.record 2, north, @cards2
      @history.record 3, north, @cards3
      expect(@history.certainToLose(Deck.twoOfDiamonds)).toBe(false)

    it "returns false if there are enough undercards to the players", ->
      @history.setPlayerCards(new Cards [Deck.fiveOfSpades])
      @history.record 1, north, @cards1
      @history.record 1, north, @cards3
      expect(@history.certainToLose(Deck.sixOfDiamonds)).toBe(false)

    it "returns false if there are enough undercards to the players considering known suits", ->
      @history.setPlayerCards(new Cards [Deck.threeOfDiamonds])
      @history.record 1, north, @cards1
      expect(@history.certainToLose(Deck.sevenOfDiamonds)).toBe(false)

  describe "heartsBroken", ->
    it "returns true when hearts have played", ->
      cards = new Cards [
        Deck.twoOfDiamonds,
        Deck.threeOfDiamonds,
        Deck.aceOfHearts,
        Deck.aceOfDiamonds
      ]
      @history.setPlayerCards(new Cards, north)
      @history.record 1, north, cards
      expect(@history.heartsBroken()).toBe(true)

    it "returns false when hearts have not played", ->
      cards = new Cards [
        Deck.twoOfDiamonds,
        Deck.threeOfDiamonds,
        Deck.kingOfDiamonds,
        Deck.aceOfDiamonds
      ]
      @history.setPlayerCards(new Cards, north)
      @history.record 1, north, cards
      expect(@history.heartsBroken()).toBe(false)

  describe "cardToSuitRatio", ->
    beforeEach ->
      @cards1 = new Cards [
        Deck.aceOfDiamonds,
        Deck.kingOfDiamonds,
        Deck.queenOfDiamonds,
        Deck.jackOfDiamonds
      ]
      @cards2 = new Cards [
        Deck.tenOfDiamonds,
        Deck.nineOfDiamonds,
        Deck.eightOfDiamonds,
        Deck.sevenOfDiamonds,
      ]
      @cards3 = new Cards [
        Deck.sixOfDiamonds,
        Deck.aceOfHearts,
        Deck.fiveOfDiamonds,
        Deck.fourOfDiamonds
      ]
      @history = new History
      @playerCards = new Cards []

    it "returns true when there are fewer undercards than are likely to play", ->
      cards = new Cards [
        Deck.twoOfDiamonds,
        Deck.threeOfDiamonds,
        Deck.aceOfHearts,
        Deck.aceOfDiamonds
      ]
      @history.setPlayerCards(@playerCards, north)
      @history.record 1, north, cards
      # consider these as though I'm the first to go next round
      expect(@history.cardToSuitRatio(Deck.sixOfDiamonds, new Cards)).toBe(1)
      expect(@history.likelyToLose(Deck.sixOfDiamonds, new Cards)).toBe(false)

      expect(@history.cardToSuitRatio(Deck.fiveOfDiamonds, new Cards)).toBe(1/2)
      expect(@history.likelyToLose(Deck.fiveOfDiamonds, new Cards)).toBe(true)

      expect(@history.cardToSuitRatio(Deck.fourOfDiamonds, new Cards)).toBe(0)
      expect(@history.likelyToLose(Deck.fourOfDiamonds, new Cards)).toBe(true)

