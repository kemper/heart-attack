Player = require('../player')
Hand = require('../hand')
Card = require('../card')
types = require('../lib/hearts_types')
Position = types.Position
Rank = types.Rank
Suit = types.Suit

describe "player", ->
  player = new Player(Position.SOUTH)
  ace_of_spades = new Card {rank: Rank.ACE, suit: Suit.SPADES}

  it "should take a position", ->
    expect(player.position).toBe(Position.SOUTH)

  it "should update the hand", ->
    hand = new Hand [ace_of_spades]
    player.receiveHand hand
    expect(player.hand).toEqual(hand)

