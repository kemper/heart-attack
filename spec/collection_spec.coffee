Collection = require('../collection')
Deck = require('../deck')

describe "Collection", ->

  it "should keep a copy of the cards", ->
    cards = [Deck.aceOfSpades]
    hand = new Collection(cards)
    expect(hand.items).toEqual(cards)
    expect(hand.items).not.toBe(cards)

  describe "add", ->
    beforeEach -> 
      @collection = new Collection([Deck.kingOfSpades])

    it "adds the card to cards", ->
      @collection.add(Deck.aceOfSpades)
      expect(@collection.items).toEqual([Deck.kingOfSpades, Deck.aceOfSpades])

    it "does not add the card when it exists", ->
      @collection.add(Deck.kingOfSpades)
      expect(@collection.items).toEqual([Deck.kingOfSpades])

    it "does not add null", ->
      @collection.add(null)
      expect(@collection.items).toEqual([Deck.kingOfSpades])

    it "does not add undefined", ->
      @collection.add(undefined)
      expect(@collection.items).toEqual([Deck.kingOfSpades])

  describe "addAll", ->
    beforeEach ->
      @collection = new Collection([Deck.kingOfSpades])

    it "adds the card to cards", ->
      @collection.addAll(new Collection([Deck.aceOfSpades, Deck.aceOfDiamonds]))
      expect(@collection.items).toEqual([Deck.kingOfSpades, Deck.aceOfSpades, Deck.aceOfDiamonds])

    it "does not add the card when it exists", ->
      @collection.addAll(new Collection([Deck.kingOfSpades, Deck.aceOfDiamonds]))
      expect(@collection.items).toEqual([Deck.kingOfSpades, Deck.aceOfDiamonds])

  describe "remove", ->
    beforeEach ->
      @initialCollection = [Deck.aceOfSpades, Deck.threeOfSpades, Deck.twoOfClubs]
      @collection = new Collection(@initialCollection)

    it "removes the card", ->
      @collection.remove(Deck.threeOfSpades)
      expect(@collection.items).toEqual([Deck.aceOfSpades, Deck.twoOfClubs])

    it "does not fail when cards doesn't exist", ->
      @collection.remove(Deck.kingOfSpades)
      expect(@collection.items).toEqual(@initialCollection)

  describe "removeAll", ->
    beforeEach ->
      @initialCollection = [Deck.aceOfSpades, Deck.threeOfSpades, Deck.twoOfClubs]
      @collection = new Collection(@initialCollection)

    it "removes the cards", ->
      @collection.removeAll(new Collection([Deck.aceOfSpades, Deck.threeOfSpades]))
      expect(@collection.items).toEqual([Deck.twoOfClubs])

    it "does not fail when cards doesn't exist", ->
      @collection.removeAll(new Collection [Deck.kingOfSpades])
      expect(@collection.items).toEqual(@initialCollection)

