Hand = require('../hand')
Deck = require('../deck')
types = require('../lib/hearts_types')

describe "Hand", ->
  describe "passing", ->
    beforeEach -> 
      @cards = [
        Deck.aceOfSpades,
        Deck.threeOfSpades,
        Deck.kingOfHearts,
        Deck.aceOfHearts,
        Deck.threeOfDiamonds,
        Deck.twoOfClubs
      ]
      @hand = new Hand(@cards)

    describe "highestThree", ->
      it "returns and removes the 3 highest cards", ->
        highest = @hand.removeHighestThree()
        expect(highest).toEqual([Deck.kingOfHearts, Deck.aceOfSpades, Deck.aceOfHearts])
        expect(@hand.cards).toEqual([Deck.twoOfClubs, Deck.threeOfSpades, Deck.threeOfDiamonds])

    describe "cards to pass", ->
      describe "dangerous spades", ->
        it "returns the queen of spades if there are fewer than 4 spades", ->
          hand = new Hand [
            Deck.aceOfSpades,
            Deck.threeOfSpades,
            Deck.queenOfSpades
          ]
          expect(hand.cardsToPass().items).toContain(Deck.queenOfSpades)

        it "returns the king of spades if there are fewer than 4 spades", ->
          hand = new Hand [
            Deck.kingOfSpades,
            Deck.threeOfSpades,
            Deck.queenOfSpades
          ]
          expect(hand.cardsToPass().items).toContain(Deck.kingOfSpades)

        it "returns the ace of spades if there are fewer than 4 spades", ->
          hand = new Hand [
            Deck.aceOfSpades,
            Deck.threeOfSpades,
            Deck.queenOfSpades
          ]
          expect(hand.cardsToPass().items).toContain(Deck.aceOfSpades)

        it "returns the ace and king of spades if there are fewer than 5 spades, and no queen", ->
          hand = new Hand [
            Deck.aceOfSpades,
            Deck.kingOfSpades,
            Deck.threeOfSpades
          ]
          expect(hand.cardsToPass().items).toContain(Deck.aceOfSpades)
          expect(hand.cardsToPass().items).toContain(Deck.kingOfSpades)

        it "returns the ace, king, queen of spades if there are fewer than 4 spades", ->
          hand = new Hand [
            Deck.aceOfSpades,
            Deck.kingOfSpades,
            Deck.queenOfSpades
          ]
          expect(hand.cardsToPass().items).toContain(Deck.aceOfSpades)
          expect(hand.cardsToPass().items).toContain(Deck.kingOfSpades)
          expect(hand.cardsToPass().items).toContain(Deck.queenOfSpades)

        it "does not return the queen, king, or ace of spades if there are 4 spades", ->
          hand = new Hand [
            Deck.aceOfSpades,
            Deck.kingOfSpades,
            Deck.queenOfSpades,
            Deck.threeOfSpades
          ]
          expect(hand.cardsToPass().items).not.toContain(Deck.aceOfSpades)
          expect(hand.cardsToPass().items).not.toContain(Deck.kingOfSpades)
          expect(hand.cardsToPass().items).not.toContain(Deck.queenOfSpades)
