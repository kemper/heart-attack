Cards = require('../cards')
Deck = require('../deck')
Collection = require('../collection')
types = require('../lib/hearts_types')
Suit = types.Suit
Rank = types.Rank
Position = types.Position

describe "Cards", ->

  describe "highestUnder", ->
    beforeEach -> 
      @cards = new Cards([Deck.kingOfSpades, Deck.twoOfHearts, Deck.threeOfClubs])

    it "returns all the cards of a given suit", ->
      expect(@cards.highestUnder(Deck.aceOfSpades)).toEqual(Deck.kingOfSpades)
      expect(@cards.highestUnder(Deck.threeOfHearts)).toEqual(Deck.twoOfHearts)
      expect(@cards.highestUnder(Deck.twoOfClubs)).toEqual(undefined)

  describe "under", ->
    beforeEach -> 
      @cards = new Cards([Deck.kingOfSpades, Deck.fourOfSpades, Deck.twoOfHearts, Deck.threeOfClubs])

    it "returns all the cards of a given suit", ->
      expect(@cards.under(Deck.kingOfSpades)).toEqual(new Cards [Deck.fourOfSpades])
      expect(@cards.under(Deck.aceOfSpades)).toEqual(new Cards [Deck.kingOfSpades, Deck.fourOfSpades])
      expect(@cards.under(Deck.threeOfHearts)).toEqual(new Cards [Deck.twoOfHearts])
      expect(@cards.under(Deck.twoOfHearts)).toEqual(new Cards)
      expect(@cards.under(Deck.kingOfSpades)).toEqual(new Cards [Deck.fourOfSpades])


  describe "all", ->
    beforeEach -> 
      cards = [Deck.aceOfSpades, Deck.threeOfSpades, Deck.twoOfClubs]
      @cards = new Cards(cards)

    it "returns all the cards of a given suit", ->
      expect(@cards.all(Suit.SPADES)).toEqual([Deck.aceOfSpades, Deck.threeOfSpades])

    it "returns empty array when suit is empty", ->
      expect(@cards.all(Suit.HEARTS)).toEqual([])

  describe "lowest", ->
    beforeEach ->
      cards = [Deck.aceOfSpades, Deck.threeOfSpades, Deck.twoOfClubs]
      @cards = new Cards(cards)

    it "returns the lowest card for the given rank", ->
      expect(@cards.lowest(Suit.CLUBS)).toBe(Deck.twoOfClubs)
      expect(@cards.lowest(Suit.SPADES)).toBe(Deck.threeOfSpades)

    it "returns the lowest card when no rank provided", ->
      expect(@cards.lowest()).toBe(Deck.twoOfClubs)

    it "returns undefined when there is no lowest card for the given rank", ->
      expect(@cards.lowest(Suit.HEARTS)).toBe(undefined)

  describe "highest", ->
    beforeEach -> 
      cards = [Deck.threeOfSpades, Deck.aceOfSpades, Deck.twoOfClubs]
      @cards = new Cards(cards)

    it "returns the highest card for the given rank", ->
      expect(@cards.highest(Suit.CLUBS)).toBe(Deck.twoOfClubs)
      expect(@cards.highest(Suit.SPADES)).toBe(Deck.aceOfSpades)

    it "returns the highest card when no rank provided", ->
      expect(@cards.highest()).toBe(Deck.aceOfSpades)

    it "returns undefined when there is no lowest card for the given rank", ->
      expect(@cards.highest(Suit.HEARTS)).toBe(undefined)

  describe "queenOfSpades", ->
    it "should return the queen when the cards have the queen of spades", ->
      cards = new Cards [Deck.threeOfSpades, Deck.queenOfSpades, Deck.twoOfClubs]
      expect(cards.queenOfSpades()).toBe(Deck.queenOfSpades)
      expect(cards.hasQueenOfSpades()).toBe(true)

    it "should return undefined when the cards does not have the queen of spades", ->
      cards = new Cards [Deck.threeOfSpades, Deck.twoOfClubs]
      expect(cards.queenOfSpades()).toBe(undefined)
      expect(cards.hasQueenOfSpades()).toBe(false)

  describe "twoOfClubs", ->
    it "should return true when the cards have the queen of spades", ->
      cards = new Cards [Deck.threeOfSpades, Deck.queenOfSpades, Deck.twoOfClubs]
      expect(cards.twoOfClubs()).toBe(Deck.twoOfClubs)
      expect(cards.hasTwoOfClubs()).toBe(true)

    it "should return true when the cards does not have the queen of spades", ->
      cards = new Cards [Deck.threeOfSpades, Deck.queenOfSpades]
      expect(cards.twoOfClubs()).toBe(undefined)
      expect(cards.hasTwoOfClubs()).toBe(false)

  describe "points", ->
    it "should return number of hearts when no queen of spades", ->
      cards = new Cards [Deck.threeOfHearts, Deck.twoOfClubs]
      expect(cards.points()).toBe(1)

    it "should return hearts plus queen of spades", ->
      cards = new Cards [
        Deck.kingOfDiamonds,
        Deck.threeOfHearts,
        Deck.queenOfSpades,
        Deck.twoOfHearts
      ]
      expect(cards.points()).toBe(15)

  describe "suits", ->
    it "returns empty collection for no cards", ->
      cards = new Cards
      expect(cards.suits()).toEqual(new Collection)

    it "returns the suits of the cards without duplicates", ->
      cards = new Cards [Deck.threeOfHearts, Deck.twoOfClubs, Deck.threeOfClubs, Deck.aceOfSpades]
      expect(cards.suits()).toEqual(new Collection [Suit.HEARTS, Suit.CLUBS, Suit.SPADES])

  describe "toString", ->
    it "displays many cards", ->
      cards = new Cards [Deck.kingOfSpades, Deck.aceOfSpades, Deck.tenOfSpades]
      expect(cards.toString()).toEqual("KS AS 10S")

  describe "sort", ->
    it "should sort the cards", ->
      cards = [Deck.kingOfSpades, Deck.aceOfSpades, Deck.tenOfSpades]
      expectedCards = [Deck.tenOfSpades, Deck.kingOfSpades, Deck.aceOfSpades]
      hand = new Cards(cards)
      hand.sort()
      expect(hand.cards).toEqual(expectedCards)

    it "should sort the cards when mixed suits", ->
      cards = [Deck.kingOfSpades, Deck.aceOfSpades, Deck.kingOfClubs, Deck.aceOfClubs]
      expectedCards = [Deck.kingOfSpades, Deck.kingOfClubs, Deck.aceOfSpades, Deck.aceOfClubs]
      hand = new Cards(cards)
      hand.sort()
      expect(hand.cards).toEqual(expectedCards)

    it "should not change a sorted deck", ->
      cards = [Deck.kingOfSpades, Deck.kingOfClubs, Deck.aceOfSpades, Deck.aceOfClubs]
      hand = new Cards(cards)
      hand.sort()
      expect(hand.cards).toEqual(cards)

