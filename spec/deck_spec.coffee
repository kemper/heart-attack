Deck = require('../deck')
types = require('../lib/hearts_types')
Card = require('../card')
Cards = require('../cards')
Rank = types.Rank
Suit = types.Suit

describe "Deck", ->
  describe "lookup", ->
    it "returns the exact card instance given the rank and suit with spades", ->
      card = new Card {rank: Rank.ACE, suit: Suit.SPADES}
      cononical_card = Deck.lookup card
      expect(cononical_card).toBe(Deck.aceOfSpades)

    it "returns the exact card instance given the rank and suit with hearts", ->
      card = new Card {rank: Rank.KING, suit: Suit.HEARTS}
      cononical_card = Deck.lookup card
      expect(cononical_card).toBe(Deck.kingOfHearts)

  describe "lookupAll", ->
    it "returns the exact card instance given the rank and suit with spades", ->
      card1 = new Card {rank: Rank.ACE, suit: Suit.SPADES}
      card2 = new Card {rank: Rank.QUEEN, suit: Suit.SPADES}
      cononical_cards = Deck.lookupAll [card1, card2]
      expect(cononical_cards).toEqual(new Cards [Deck.aceOfSpades, Deck.queenOfSpades])
