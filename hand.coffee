types = require('./lib/hearts_types')
Cards = require('./cards')
Suit = types.Suit
Rank = types.Rank
Position = types.Position
Deck = require('./deck')
#
# holds the hand
#
# can
#   count suit remaining
#   sort, aces highest
#   queen_of_spades
#   two_of_clubs
#
class Hand extends Cards
  removeHighestThree: ->
    @sort().splice(@cards.length - 3, @cards.length + 1)

  # TODO: pass 2 of clubs, unless clubs are a low risk suit
  cardsToPass: ->
    cardsToPass = new Cards
    if @all(Suit.SPADES).length < 4
      if @has(Deck.aceOfSpades)
        cardsToPass.add(Deck.aceOfSpades)
      if @has(Deck.kingOfSpades) 
        cardsToPass.add(Deck.kingOfSpades)
      if @has(Deck.queenOfSpades)
        cardsToPass.add(Deck.queenOfSpades)

    else if @all(Suit.SPADES).length < 5
      if @has(Deck.aceOfSpades) and @has(Deck.kingOfSpades) and !@has(Deck.queenOfSpades)
        cardsToPass.add(Deck.aceOfSpades)
        cardsToPass.add(Deck.kingOfSpades)

    remaining = 3 - cardsToPass.size()

    otherSuits = new Cards
    otherSuits.addAll(new Cards @all(Suit.HEARTS))
    otherSuits.addAll(new Cards @all(Suit.CLUBS))
    otherSuits.addAll(new Cards @all(Suit.DIAMONDS))

    if remaining > 0
      for i in [1..remaining]
        highest = otherSuits.highest()
        cardsToPass.add(highest)
        otherSuits.remove(highest)

    cardsToPass

module.exports = Hand
