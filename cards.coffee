types = require('./lib/hearts_types')
Collection = require('./collection')
Suit = types.Suit
Rank = types.Rank
Position = types.Position

class Cards extends Collection
  @sort: (cards) ->
    cards.sort (a,b) ->
      return Cards.compareRank(a,b)
    cards

  @compareRank: (a,b) ->
    return 0 if a.rank == b.rank
    return 1 if a.rank == Rank.ACE
    return -1 if b.rank == Rank.ACE
    return -1 if a.rank < b.rank
    return 1 if a.rank > b.rank

  constructor: (cards) ->
    super(cards)
    @cards = @items

  sort: ->
    Cards.sort(@cards)

  all: (suit) ->
    (card for card in @cards when card.suit == suit)

  lowest: (suit=undefined) ->
    if suit?
      Cards.sort(@all(suit))[0]
    else
      Cards.sort(@cards)[0]

  highest: (suit=undefined) ->
    if suit?
      Cards.sort(@all(suit)).reverse()[0]
    else
      Cards.sort(@cards).reverse()[0]

  highestUnder: (card) ->
    @under(card).highest()

  under: (card) ->
    same_suit = new Cards(@all(card.suit))
    undercards = new Cards
    for suit_card in same_suit.cards
      undercards.add(suit_card) if Cards.compareRank(suit_card, card) < 0
    undercards

  suits: ->
    suits = new Collection
    (suits.add(card.suit) for card in @cards)
    suits

  hasQueenOfSpades: ->
    @queenOfSpades()?

  queenOfSpades: ->
    (card for card in @cards when card.suit == Suit.SPADES and card.rank == Rank.QUEEN)[0]

  hasTwoOfClubs: ->
    @twoOfClubs()?

  twoOfClubs: ->
    (card for card in @cards when card.suit == Suit.CLUBS and card.rank == Rank.TWO)[0]

  points: ->
    points = 0
    points = 13 if @hasQueenOfSpades()
    points += @all(Suit.HEARTS).length

  toString: ->
    (card.toString() for card in @cards).join " "

module.exports = Cards
