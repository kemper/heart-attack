types = require('./lib/hearts_types')
Rank = types.Rank
Suit = types.Suit

class Card
  rankMap: (->
    rankMap = {}
    rankMap[Rank.ACE] =   "A"
    rankMap[Rank.KING] =  "K"
    rankMap[Rank.QUEEN] = "Q"
    rankMap[Rank.JACK] =  "J"
    rankMap[Rank.TEN] =   "10"
    rankMap[Rank.NINE] =  "9"
    rankMap[Rank.EIGHT] = "8"
    rankMap[Rank.SEVEN] = "7"
    rankMap[Rank.SIX] =   "6"
    rankMap[Rank.FIVE] =  "5"
    rankMap[Rank.FOUR] =  "4"
    rankMap[Rank.THREE] = "3"
    rankMap[Rank.TWO] =   "2"
    rankMap
  )()

  suitMap: (->
    suitMap = {}
    suitMap[Suit.HEARTS] =   "H"
    suitMap[Suit.SPADES] =   "S"
    suitMap[Suit.CLUBS] =    "C"
    suitMap[Suit.DIAMONDS] = "D"
    suitMap
  )()

  constructor: (fields) ->
    @rank = fields.rank
    @suit = fields.suit

  toString: ->
    @rankMap[@rank] + @suitMap[@suit]


module.exports = Card
